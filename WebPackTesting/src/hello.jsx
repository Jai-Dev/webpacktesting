﻿
'use strict'
var React = require('react')
module.exports = React.createClass({
    displayName: 'HelloReact',
    render: function(){
    	return (
			<div className="panel panel-default">
				<div className="panel-body">Hello from React</div>
			</div>
		);
    }
})