﻿
'use strict';
var React = require('react');
var ReactDOM = require('react-dom');
var Hello = require('./Hello');
var ResourceTile = require('./resourceTile')
var Content = require('./content');

var clientSettings = {
	submit: "Save",

};

var domContainer = document.getElementById('content');

tess.renderTile = function () {
	ReactDOM.unmountComponentAtNode(domContainer)
	ReactDOM.render(<ResourceTile names={clientSettings } />, domContainer);
};

tess.renderHello = function () {
	ReactDOM.unmountComponentAtNode(domContainer)
	ReactDOM.render(<Hello />, domContainer);
};

tess.clear = function () {
	ReactDOM.unmountComponentAtNode(domContainer)
};



//This just showing the require stuff is working.
document.getElementById('otherContent').innerHTML = Content;

