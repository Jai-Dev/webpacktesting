﻿
'use strict'
var React = require('react');



var ResourceTile = React.createClass({
	displayName: 'ResourceTile',
	
	getInitialState: function(){
		return {
			content: "Loading...",
			processing: "Processing..."
		};
	},

	getDefaultProps: function () {
		return {
			names:
			{
				submit: "Submit",
			},
			heading: "This is the Title"
		};
	},

	componentDidMount() {
		this.getData();
	},

	componentWillMount(){
		tess.doSomething = (data) => {
			this.showResults("Set from outside react " + new Date());
		};
	},

	doAlert: function(msg){
		alert(msg);
	},

	showResults: function(data){
		this.setState(
			{
				content: data,
				processing: ""
			});
	},

	getData: function () {
		//Simulate an async jquery ajax request using promises.
		var that = this;
		this.setState({processing: "Processing..."});
		var d = $.Deferred(function (d) {
			setTimeout(function () {
				d.resolve("Dynamic content " + new Date());
			}, 500);
		});

		d.then(function (data) {
			//that.doAlert(data);
			that.showResults(data);
		});
	},

	render: function () {
		return (
			<div className="panel panel-default">
				<ResourceHeader heading={this.props.heading}/>
				<ResourceContent content={this.state.content}/>
				<ResourceFooter submitText={this.props.names.submit} handleClick={this.getData} processing={this.state.processing} />
			</div>
		);
	}
});

var ResourceHeader = React.createClass({
	render: function () {
		return (
			 <div className="panel-heading">{this.props.heading}</div>
		);
	}
});

var ResourceContent = React.createClass({
	render: function () {
		return (
			 <div className="panel-body">{this.props.content}</div>
		);
	}
});

var ResourceFooter = React.createClass({
	render: function () {
		//var buttonSwitch = this.state.active ? "Processing" : "";
		return (
			 <div className="panel-footer">
				<button type="button" className="btn btn-info" onClick={this.props.handleClick}>{this.props.submitText}</button>
				&nbsp;{this.props.processing}
			 </div>
		);
	}
});

module.exports = ResourceTile;