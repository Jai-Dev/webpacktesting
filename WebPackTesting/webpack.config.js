﻿/// <binding ProjectOpened='Watch - Development' />
"use strict";
var path = require('path');

var srcDir = path.join(__dirname, 'src');
console.log("srcDir:" + srcDir);

module.exports = {
    entry: "./src/index.jsx",
    output: {
        filename: "./dist/bundle.js"
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/, //this regular expression includes .js and jsx
                include: srcDir,
                exclude: /node_modules/,
                loader: "babel-loader",

                // Options to configure babel with
                query: {
                	presets: ['es2015', 'react'],
                	cacheDirectory: path.join(__dirname, 'obj/BabelCache') //This speeds rebuilds. Use the ASP.net obj directory.
                }
            }
        ]
    }
	,

    resolve: {
        extensions: ['', '.js', '.jsx']
    }
};